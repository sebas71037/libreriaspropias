/**
* Bloods v1.0
* autor: naitssva7103 - Sebastian Vargas Arenas
* Date: Thursday January 27, 2016
* Description: A library to create, manipulate , and animate the frontend of a web page
* Contact: email [sebas71037[@]gmail.com - sebas_7103[@]hotmail.com]
* License: Apache 2.0
*/

var Bloods = function(){

}

//Bloods.aux = {};

/**
* Adiciona un elemento clonado a una etiqueta especifica
* @param obj: botón que crea el evento (add)
* @param selector: string o objeto que se clonara
* @param couple: pareja del botón que crea el evento (delete)
* @param max: máximo de objetos que se pueden llegar a clonar
* @param display: como se comportara el boton que crear el evento
**/
Bloods.addElement = function(obj, selector, couple, max, display){
	display = display || "block";
	var select = $(selector);
	if( select.length < max){
			var form = select[0].parentElement;;
			var newSelect = select[0].cloneNode(true);	
			form.insertBefore(newSelect, form.children[select.index()]);
		if(select.length == max-1){
			obj.style.display = "none";
		}
		if($(couple)[0].style.display == "none" || $(couple)[0].style.display == ""){
			$(couple)[0].style.display = display;
		}
	}
}

/**
* Convierte un array(s) a un objeto
* @param array: arrglo
* @param nivel: en el caso de ser array de array y se quiere convertir en objetos hasta 2 niveles de profundidad
*/
Bloods.arrayToObject = function(array, nivel){
	nivel = nivel || false;
	var obj = {};
		for (var i = 0; i < array.length; i++) {
			if(nivel && this.toType(array[i]) == "array"){
				var objSub = {};
				for (var j = 0; j < array[i].length; j++) {
					objSub[j] = array[i][j];
				};
				obj[i] = objSub;
			}else{
				obj[i] = array[i];
			}
		};	
	return obj;
}

/** 
* Revisa que un parametro cumpla con una restricción
* @param url: dirección donde se quiere acceder de forma asincrónica
* @param method: metodo de envío
* @param obj: objeto u objetos de los que seran extraídos sus valores para el envío
*/
Bloods.checkParameters = function(url, method, obj, data, callback){
	data = data || false;
	if(obj.length == undefined && !data){
		data = {};
		data[obj.name] = obj.value;
	}else if(!data){
		data = this.formToJson(obj);
	}
	$.ajax({
		url: url,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: method,
	}).done(function(r){
		var response = JSON.parse(r);
		if(callback){
			callback(response);
		}
	});
}

/**
* Cambia la opciones de un select de todos los select igual al [param = selector] evitando doble selección de una misma opción
* @param obj: opción selecionada
* @param selector: selects que tienen las misma opciones
*/
Bloods.changeOption = function(obj, selector){
	var select = $(selector);
	for(var i = 0; i < select.length; i++){
		if($(select[i]).index() == $(obj).index() && obj.value != ""){
			select[i].setAttribute("data-select",obj.value);
		}
		if(obj.value != ""){
			select[i].children[obj.value].style.display = "none";			
		}else{
			select[i].children[obj.getAttribute("data-select")].style.display = "block";
		}
	}
}

/*
* @autor: URL: http://stackoverflow.com/questions/728360/most-elegant-way-to-clone-a-javascript-object?answertab=votes#tab-top
*/

Bloods.clone = function clone(obj) {
    var copy;
    if (null == obj || "object" != typeof obj) return obj;
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

/**
* @param vars: atributos
* @param dom: tipo de elemento a crear
* @param objF: padre donde se almacenara el nuevo elemento
*/
Bloods.createElement = function(vars, dom, objF){
	var element = document.createElement(dom);
	this.setAttributes((vars["vars"] != undefined)? vars["vars"]:vars, element);
	objF[0].appendChild(element);
	if(vars.children != undefined){
			this.setChilds(vars.children,$(element));
	}
}

/**
* Devuelve el indice de un array de objetos(dom) através de una objeto(dom) como objetivo
* @param vars: array de objetos(dom)
* @param target: objeto(dom) objetivo
*/
Bloods.compareHTMLDom = function (vars, dom){
    for (var i = 0; i < vars.length; i++) {
        if(vars[i] === dom){
            return i;
        }
    }
    return -1;
}

/*
* convierte un valor a hexadecimal
* @param c: valor
*/
Bloods.componentToHex = function (c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

/**
* @param *: su constructor funciona de la misma que la [function = addElement]
*/
Bloods.delElement = function(obj, selector, couple, display){
	display = display || "block";
	var select = $(selector);
	if( select.length > 1){
		select[select.length-1].remove();
		if(select.length == 2){
			obj.style.display = "none";
		}
		if($(couple)[0].style.display == "none"){
			$(couple)[0].style.display = display;
		}
	}
}

/**
* Convierte los hijos de un formulario en un Object sin los elementos de [type = submit and type = ""]
* @param obj: formulario
*/
Bloods.formToJson = function(obj){
	var vars = {};
	for (var i = 0; i < obj.length; i++) {
		if(obj[i].type != "submit" && obj[i].value != ""){
			var name = (obj[i].name != undefined)? obj[i].name : i;
			vars[name] = obj[i].value;
		}

	};
	return vars;
}

/*
* @autor: ashleyford [http://papermashup.com/read-url-get-variables-withjavascript/]
*/
Bloods.getUrlVars = function() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	vars[key] = value;
	});
	return vars;
}

/*
* Convierte un color hexadecimal y lo convierte en rgb
* @param hex: color hexadecimal
* @param type: el tipo de respuesta del color rgb
* @param alpha: nivel de alfa para el caso rgba
*/
Bloods.hexToRGB =  function(hex, type, alpha){
	type = type || false;
	alpha = alpha || 0.01;
    var a = (hex.search("#") == -1)? hex.match(/.{1,2}/g) : hex.split("#")[1].match(/.{1,2}/g), response = "";
    switch(type){
    	case "rgb": 
	    	response = "rgb("+parseInt(a[0], 16)+","+parseInt(a[1], 16)+","+parseInt(a[2], 16)+")";
	    	break;
    	case "rgba":
	    	response = "rgba("+parseInt(a[0], 16)+","+parseInt(a[1], 16)+","+parseInt(a[2], 16)+","+alpha+")";
	    	break;
    	case "array": 
	    	response = [parseInt(a[0], 16),parseInt(a[1], 16),parseInt(a[2], 16)];
	    	break;
    	default: 
	    	response = parseInt(a[0], 16)+","+parseInt(a[1], 16)+","+parseInt(a[2], 16);
    }
    return response;
}

/**
* Devuelve el indice de un array de objetos(dom) através de una clase como objetivo
* @param vars: array de objetos(dom)
* @param target: clase objetivo
*/
Bloods.indexOfElements = function(vars, target){
	for (var i = 0; i < vars.length; i++) {
		if($(vars[i]).hasClass(target)){
			return i;
		}
	}
	return -1;
}

/*
* @autor: Sean Vieira [http://stackoverflow.com/users/135978/sean-vieira]
*/
Bloods.isEmpty = function (obj) {
    if (obj == null) return true;
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }
    return true;
}

/**
* Anima un Slider creado con un panel de display flex, que sirva como panel de opciones
* @param obj: boton Right o Left para mover el slider
* @param time: tiempo de animación
*/
Bloods.moveSliderFlex =  function(obj, time){
	time = time || 1000;
	var signo = (obj.getAttribute("data-O") == "left")? -1: 1;
	var content = $('.'+obj.getAttribute("targetC")), index = parseInt(content[0].getAttribute("data-P")) , children = $('.'+obj.getAttribute("targetI")), v = (content[0].scrollWidth-$(content.parent()).width())/(children.length-parseInt(content[0].getAttribute("data-V"))), num = v*(index) + (signo*v);
  	content.animate({
    	scrollLeft: num
  	},time);
  	content[0].setAttribute("data-P", (signo == -1 && index > 0)? index-1 : (signo == 1 && index < children.length-content[0].getAttribute("data-V"))? index +1 : index );
}

/**
* Convierte un Objecto(s) o etiqueta(s) a un Arreglo
* @param obj: Object
* @param properties: es una array de las propiedades que quieres poner en el array 
*/
Bloods.objectToArray = function(obj, properties){
	properties = properties || false;
	var array = [];
	if(!properties){
		array = $.map(obj, function(value, index) {
			return [value];
		});
	}else{
		obj = (obj.length == undefined)? [obj] : obj;
		for (var i = 0; i < obj.length; i++) {
			var arraySub = [];
			for (var j = 0; j < properties.length; j++) {
				arraySub.push(obj[i][properties[j]]);
			};	
			array.push(arraySub);
		};
		array = (properties.length == 1)? [].concat.apply([], array) : array;
	}
	return array;
}

/*
* Prepara un array de objetos de datos que seran objetos(dom) o contenidos para que sean creados por la funcion "setChilds", ligados por llaves como restricciones para el recorrido de los ciclos de creación
* @return: retorna un objeto con la función "prepare"
* @function prepare: la que ejecuta todo el procedimiento explicado 
* @function children: esta función ayuda a "prepare" a detectar y procesar lo datos de objetos que seran hijos del objeto actual en el proceso de creación
*/
Bloods.prepareContents = function(){
	var aux = {}, children = function(obj){
		var b = [];
		for (var j = 0; j < obj.children.length; j++) {
			var c = prepare(obj.children[j]);
			b.push(c.shift());
		}
		return b;
	},prepare = function(obj){
		var vars = [];
		if(obj.length != undefined){
			for (var i = 0; i < obj.length; i++) {
				vars.push(prepare(obj[i]).shift());
			}
		}else{ 
			for (var i = 0; i < obj["lengthC"]; i++) {
				var a = {};
				a["vars"] = Bloods.clone(obj.vars);
				a["dom"] = obj.dom;
				if(obj.aum !=  undefined){
					aux[obj.aum] = (aux[obj.aum] == undefined)? 0 : aux[obj.aum] + 1;
				}
				if(obj.constraint != undefined){
					a["vars"][obj.constraint["property"]] = (obj.constraint["text"] != undefined)? (aux[obj.constraint["reference"]]+ 1).toString() : window[obj.constraint["var"]][aux[obj.constraint["reference"]]];
				}
				if(obj.children != undefined){
					a["children"] = children(obj);
				}
				vars.push(a);
			}
		}
		return vars;
	}
	return{
		prepare : prepare,
	};
}(); 

/*
* Recibe un FormData y prepara todos los archivos que se han cargado en un input[type='file']
* @param data: FormData
* @param inputFile: input[type='file']
* @param nameFiles: nombre clave para los indices de los archivos
*/
Bloods.prepareDataform = function prepareDataform(data, inputFile, nameFiles){
    nameFiles = nameFiles || "file-";
    $.each(inputFile[0].files, function(i, file) {
        data.append(nameFiles+i, file);
    });
}

/**
* @param vars: atributos de los elementos a borrar
* @param obj: elemento creado
*/
Bloods.removeAttributes = function(vars, obj){
	if(vars["class"] != undefined){
		$(obj).removeClass((Bloods.toType(vars["class"]) == "array")? vars["class"].join(" ") : vars["class"]);
	}
	for(var i = 0; i < vars["attributes"].length; i++){
		obj.removeAttribute(vars["attributes"][i]);	
	}
}


/*
* Convierte un color rgb y lo convierte en hexadecimal
* @param hex: color rgb
* @param type: el tipo de respuesta del color hexadecimal
*/
Bloods.rgbToHex = function (rgb,type) {
	type =  type || false;
	if(rgb.search("rgba") != -1){
		rgb = rgb.split("(")[1].split(")")[0].split(",");
	}else if(rgb.search("rgb") != -1){
		rgb = rgb.split("(")[1].split(")")[0].split(",");
	}else if(rgb.search(",") != -1){
		rgb = rgb.split(",");
	}
	var response = this.componentToHex(parseInt(rgb[0])) + this.componentToHex(parseInt(rgb[1])) + this.componentToHex(parseInt(rgb[2]));
    return (type)?"#"+response: response;
}

/*
* Ejecución asincronica mediante ajax para un proceso que involucre la manipulcación de archivos
* @param data: FormData
* @param method: metodo de envio de archivos y variables
* @param url: dirección 
* @param callback: callback para ejecutar algo después
*/
Bloods.saveFiles = function saveFiles(data, method, url, callback){
	callback = callback || function(){};
    $.ajax({
        url: url,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: method,
        success: callback
    });
}

/**
* @param vars: atributos de los elementos a crear
* @param obj: elemento creado
*/
Bloods.setAttributes =  function(vars, obj){
	for(var k in vars){
		if(k == "class"){
			obj.className += (Bloods.toType(vars[k]) == "array")? " "+vars[k].join(" ") : vars[k];
		}else if(k == "html"){
			obj.innerHTML = vars[k];
		}else if(k == "style"){
			for (var i in vars[k]) {
				obj.style[i] = vars[k][i];
			}	
		}else{
			obj.setAttribute(k,vars[k]);	
		}
	}
}

/**
* @param vars: arreglo de objetos con los atributos de los elementos a crear
* @param objF: objeto donde se almacenaran los elementos a crear
* @param dom: el tipo de los elementos que se crearan
* @param add: condicional para adicionar en vez de sobreescribir
*/
Bloods.setChilds = function(vars, objF, dom, add){
	objF = objF || false;
	dom = dom || false;
	add = add || false;
	if(objF && dom){
		(objF[0].nodeName == "SELECT")? (add == false)? $(objF).children().not(":first-child").remove(): false : (add == false)? objF[0].innerHTML = "" : false;
	}
    for (var i = 0; i < vars.length; i++) {
    	this.createElement(vars[i], (dom)? dom : vars[i].dom, (objF)? objF : $("."+vars[i].parent));
    };
}

/**
* @param url: dirección donde se quiere acceder de forma asincrónica
* @param method: metodo de envío
* @param obj: objeto que tiene la key para el envío
* @param data: data de la llamada asincronica
* @param objF: objeto donde seran agregados lo datos que lleguen del envío
* @param dom: el tipo de los elementos que se crearan
* @param add: condicional para adicionar en vez de sobreescribir
*/
Bloods.setAsynData = function(url, method, obj, data, objF, dom, add, callback){	
	data =  data || false;
	objF = objF || false;
	dom = dom || false;
	add = add || false;
	if(obj.value != ""){
        if((obj.length == undefined || obj.nodeName == "SELECT") && !data){
        	data = {}
			data[obj.name] = obj.value;
		}else if(!data){
			data = this.formToJson(obj);
		}

        $.ajax({
		    url: url,
	        data: data,
	        cache: false,
	        contentType: false,
	        processData: false,
	        type: method,
    	}).done(function(r){
            var vars = JSON.parse(r);
            Bloods.setChilds(vars, objF, dom, add);
	        if(callback){
				callback(true);
			}
        });
    }else{
    	$(objF).children().not(":first-child").remove();
    }
}

/*
* Lee el source de un archivo y lo devuelve en una función callback para manipularlo al gusto del desarrollador
* @param file: archivo
* @param onLoadCallback: función callback
*/
Bloods.sourceInputFile =  function(file, onLoadCallback){
    var reader = new FileReader();
    reader.onload = onLoadCallback;
    reader.readAsDataURL(file);
};

/**
* @autor Angus Croll
*/
Bloods.toType = function(obj) {
  return ({}).toString.call(obj).match(/\s([a-z|A-Z]+)/)[1].toLowerCase()
}

/*
* Convierte el primer caracter de un string en mayúscula
* @param obj: String
*/
Bloods.ucfirst = function(obj){
	return obj.charAt(0).toUpperCase()+obj.slice(1).toLowerCase();
}

/**
* Cambia una la clase de una etiqueta u objeto por otra 
* @param obj: Objeto que activa el evento
*/
Bloods.updateClass = function(obj){
	if(obj.className.search(obj.getAttribute('data-class1')) > -1){
		$(obj).removeClass(obj.getAttribute('data-class1'));
		obj.className += " "+obj.getAttribute('data-class2');
	}else{
		$(obj).removeClass(obj.getAttribute('data-class2'));
		obj.className += " "+obj.getAttribute('data-class1');
	}
}

/**
* Cambia una clase de un objeto de un panel de opciones, sirviendo como seleccion única de un objeto
* @param obj: Objeto que activa el evento
* @param add: Estado que permite agregar clase a las que ya disponga el objeto objetivo, sino la reescribre
*/
Bloods.updateSelected = function(obj, add, callback){
	add = add || false;
	var tarjetObj = ($(obj).hasClass(obj.getAttribute("target")))? $(obj) : $("."+$(obj)[0].className+":nth-child("+($(obj).index()+1)+") ."+obj.getAttribute("target"));	
	tarjetObj.removeClass(obj.getAttribute('data-class1'));
	var newClass = (add)? tarjetObj[0].className+" ":"";
	$.each($('.'+obj.getAttribute('data-class2')), function(){
		$(this).removeClass(obj.getAttribute('data-class2'));
		var newClassTarjet = (add)? $(this)[0].className+" ":"";
		$(this)[0].className = newClassTarjet+obj.getAttribute('data-class1');
	});
	tarjetObj[0].className = newClass+obj.getAttribute('data-class2');
	if(callback){
		callback(true);
	}	
}