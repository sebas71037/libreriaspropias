/**
* FV v1.0.0.0
* author: * naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
* Date: Tuesday October 11, 2016
* Description: A library to validation types of form
* License: Apache 2.0
*/	
var $FV = function(){

}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.getResponseValidate = function(r, def, msg){
	r.status = true;
    r.msg = (msg === null || msg == '')? def : msg;
}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.initErrors = function(id){
	var _parent = document.getElementById(id);
	var _errors = _parent.getElementsByClassName("_error");
	for (var i = 0; i < _errors.length; i++) {
		_errors[i].style.display = 'none';
	}
}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.prepareFiles = function(obj, b, callback){
	for (var i = 0; i < obj.files.length; i++) {
		if(callback){
			callback(i, obj.files[i]);	
		}else{
			b.append(obj.name+'['+i+']', obj.files[i]);
		}
	}
}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.prepareForm = function(a, b){
	for (var i = 0; i < a.length; i++) {
		switch(a[i].type){
			case 'file':
				$FV.prepareFiles(a[i], b);
				break;
			default:
				b.append(a[i].name, a[i].value);
				break;
		}
	}
}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.showErrors = function(obj){
	for (var i = 0; i < obj.length; i++) {
		var target = obj[i];
		target.obj_error.style.display = 'block';
		target.obj_error.innerHTML = target.msg;
	}
}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.validate = function(obj, v){
	v = v || obj.value;
	var r = {status: false, msg: ''};
	var _requireds = obj.getAttribute('data-required-values');
	_requireds = (_requireds === null || _requireds == '')? [] : _requireds.split(',');
	for (var i = 0; i < _requireds.length; i++) {
		r = $FV.validateCase(obj, _requireds[i], v);
		if(r.status === true){
			return r;
		}
	}
	return r;
}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.validateCase = function(obj, required, v){
	v = v || obj.value;
	var r = {status:false, msg:''};
	switch(required){
		case 'required':
			v = (typeof v == 'string' )? v.trim() : v;
			if(v === '' || v === undefined || v === false){
				$FV.getResponseValidate(r, 'Este campo es requerido', obj.getAttribute('error-required'));
			}
			break;
		case 'email':
			var patron = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		if(!patron.test(v)){
    			$FV.getResponseValidate(r, 'Correo no válido.', obj.getAttribute('error-email'));
    		}
			break;
		case 'min':
			var min = obj.getAttribute('data-min-s');
			if(min !== null && parseInt(min) !== NaN){
				if(v.toString().length < parseInt(min)){
					$FV.getResponseValidate(r, 'Digita mínimo '+min+' dígitos.', obj.getAttribute('error-min'));
				}
			}
			break;
		case 'max':
			var max = obj.getAttribute('data-max-s');
			if(max !== null && parseInt(max) !== NaN){
				if(v.toString().length > parseInt(max)){
					$FV.getResponseValidate(r, 'Digita máximo '+max+' dígitos.', obj.getAttribute('error-max'));
				}
			}
			break;
		case 'number':
			v = (typeof v == 'string' )? v.trim() : v;
			if(v !== '' || v !== undefined || v !== false){
			var patron = /^\d+$/.test(v);
				if(!patron){
					$FV.getResponseValidate(r, 'Digita solo números.', obj.getAttribute('error-number'));	
				}
			}
			break;
	}
	return r;
}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.validateField = function(obj, b, n, v){
	n = n || obj.name;
	v = v || obj.value;
	var r = $FV.validate(obj, v);
	if(b !== false){
		b.append(n, v);	
	}
	return r;
}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.validateFiles = function(obj, b){
	var r = {};
	if(obj.files.length > 0){
			$FV.prepareFiles(obj, b, function(i, e){
				r =	$FV.validateField(obj, b, obj.name+'['+i+']', e);
			});
	}else{
		r = $FV.validateField(obj, b, '');
	}
	return r;
}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.validateInputs = function(a, id, b, _view){
	_view =  _view || true;
	b = b  || false;
	var _validates = [];
	$FV.initErrors(id);
	for (var i = 0; i < a.length; i++) {
		switch(a[i].type){
			case 'file':
				$FV.validatesRequireds($FV.validateFiles(a[i], b), _validates, a[i], id);
				break;
			case 'checkbox':
				var val = (a[i].checked === false)? ' ':true;
				$FV.validatesRequireds($FV.validateField(a[i], b, false, val), _validates, a[i], id);
				break;
			case 'radio':
				//........................
				break;
			default:
				$FV.validatesRequireds($FV.validateField(a[i], b), _validates, a[i], id);
				break;
		}
	}

	if(_view === true){
		$FV.showErrors(_validates);
	}
	return _validates;
}

/**
* author: naitssva7103 - Sebastian Vargas Arenas - [sebas71037[@]gmail.com - sebas_7103@hotmail.com]
*/
$FV.validatesRequireds = function(a, _v, obj, id){
	if(a.status === true){
		var element = document.getElementById(id).querySelectorAll('[data-target-s="'+obj.name+'"]')[0];
		if(obj.name == ""){
			throw '[Error 1]: El atributo name no puede esta vacío.';
		}else if(element === undefined){
			throw '[Error 2]: El elemento _error del campo name="'+obj.name+'" no fue encontrado.';
		}else{
			_v.push({
				msg: a.msg,
				obj_error: element
			});	
		}
	}
}